package MelnHomeWorks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Homework3 {
    public static void main(String[] args) {
        Realization realization = new Realization();
        //realization.chet();
        realization.minim();
    }
}

class Realization {
    Scanner scanner = new Scanner(System.in);

    void minim() {
        List min = new ArrayList();
        int inputNumber = scanner.nextInt();
        while (inputNumber != -1) {
            min.add(inputNumber);
            inputNumber = scanner.nextInt();
        }
        Collections.sort(min);
        System.out.println(min.get(0));
    }

    void chet() {
        int chet = 0, nechet = 0;
        int inputNumber = scanner.nextInt();
        while (inputNumber != -1) {
            if (inputNumber % 2 == 0) {
                chet++;
            } else {
                nechet++;
            }
            inputNumber = scanner.nextInt();
        }
        System.out.println("Всего четных:=" + chet);
        System.out.println("Всего не четных:=" + nechet);
    }
}
