package MelnHomeWorks.Homework7;

public class Programmer extends Worker {
    public Programmer(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    public void goToVacation(int days) {
        System.out.println(name + " " + lastName + " работает " + profession + " уходит в отпуск на " + days + " дней ");
        System.out.println("Во время отпуска улетает на юг, где смотрит видео на ютубе");
    }


    public void goToWork() {
        System.out.println(name + " " + lastName + " работает " + profession);
        System.out.println("На работе содится за компьютер, пишет код, много пишет, пьет кофе и пытается не заснуть");
    }
}
