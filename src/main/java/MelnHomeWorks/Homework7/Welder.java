package MelnHomeWorks.Homework7;

public class Welder extends Worker {
    public Welder(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    public void goToVacation(int days) {
        System.out.println(name + " " + lastName + " работает " + profession + " уходит в отпуск на " + days + " дней ");
        System.out.println("Во время отпуска уезжает в деревню");
    }


    public void goToWork() {
        System.out.println(name + " " + lastName + " работает " + profession);
        System.out.println("На работе, вечно что то варит");
    }
}
