package MelnHomeWorks.Homework7;

public class Cabbie extends Worker {

    public Cabbie(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    public void goToVacation(int days) {
        System.out.println(name + " " + lastName + " работает " + profession + " уходит в отпуск на " + days + " дней ");
        System.out.println("Во время отпуска сидит дома");
    }


    public void goToWork() {
        System.out.println(name + " " + lastName + " работает " + profession);
        System.out.println("На работе возит пассажиров");
    }
}
