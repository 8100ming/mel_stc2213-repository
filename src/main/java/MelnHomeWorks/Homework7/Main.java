package MelnHomeWorks.Homework7;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        final List<Worker> workerList = new LinkedList<>();
        workerList.add(new Programmer("Alex", "Mels", "Programmer"));
        workerList.add(new Cabbie("Sem", "Wrays", "Cabbie"));
        workerList.add(new SisAdmin("Jem", "Les", "SisAdmin"));
        workerList.add(new Welder("Jim", "Lanes", "Welder"));

        for (Worker worker : workerList) {
            System.out.println("*********************************************************");
            worker.goToWork();
            worker.goToVacation(12);
        }
    }
}
