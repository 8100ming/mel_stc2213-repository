package MelnHomeWorks.Homework7;

public class Worker {
    protected String name;
    protected String lastName;
    protected String profession;

    public Worker(String name, String lastName, String profession) {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
    }

    public Worker() {
    }


    public void goToWork() {
        System.out.println(name + " " + lastName + " работает " + profession);
    }

    public void goToVacation(int days) {
        System.out.println(name + " " + lastName + " работает " + profession + " уходит в отпуск на " + days + " дней ");
    }
}
